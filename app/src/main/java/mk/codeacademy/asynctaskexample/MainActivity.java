package mk.codeacademy.asynctaskexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TEXT_STATE = "currentText";

    TextView textView;
    Button textViewExecute, textViewStopExecute;
    ProgressBar progressBar;
    ImageView imageView;

    PorgressAsyncTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar);
        textViewExecute = findViewById(R.id.buttonExecuteProgress);
        textViewStopExecute = findViewById(R.id.buttonStopExecuteProgress);
        imageView = findViewById(R.id.sourceImageView);

        // Restore TextView if there is a savedInstanceState
        if(savedInstanceState!=null){
            textView.setText(savedInstanceState.getString(TEXT_STATE));
        }

    }

    public void startTask(View view) {
        new SimpleAsyncTask(textView).execute();
    }

    public void startTaskProgress(View view) {
        myTask = new PorgressAsyncTask(textView, progressBar, textViewExecute, textViewStopExecute);
        myTask.execute(10);
    }

    public void stopTaskProgress(View view) {
        myTask.cancel(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the state of the TextView
        outState.putString(TEXT_STATE, textView.getText().toString());
    }

    public void downloadImage(View view) {
        String url = "https://java.sogeti.nl/JavaBlog/wp-content/uploads/2009/04/android_icon_256.png";
        new DownloadImageAsyncTask(this, imageView).execute(url);
    }
}
