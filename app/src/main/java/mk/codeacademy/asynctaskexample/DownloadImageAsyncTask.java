package mk.codeacademy.asynctaskexample;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.lang.ref.WeakReference;

public class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

    ProgressDialog progressDialog;
    WeakReference<ImageView> imageView;
    Context context;

    DownloadImageAsyncTask(Context context, ImageView view){
        this.context = context;
        this.imageView = new WeakReference<>(view);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(context, "Downloading", "Downloading..Please Wait", true);
    }

    @Override
    protected Bitmap doInBackground(String... strings) {

        String urldisplay = strings[0];
        Bitmap bitmap = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return bitmap;

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        progressDialog.dismiss();

        if(bitmap != null){
            imageView.get().setImageBitmap(bitmap);
        }
    }

}