package mk.codeacademy.asynctaskexample;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.lang.ref.WeakReference;

public class PorgressAsyncTask extends AsyncTask<Integer, Integer, String> {

    private final String ASYNC_TASK_TAG = "ASYNC_TASK";

    private WeakReference<TextView> textView;
    private WeakReference<ProgressBar> progressBar;
    private WeakReference<Button> startExecuteButton;
    private WeakReference<Button> stopExecuteButton;

    PorgressAsyncTask(TextView tv, ProgressBar progBar, Button startExecute, Button stopExecute) {
        this.textView = new WeakReference<>(tv);
        this.progressBar = new WeakReference<>(progBar);
        this.startExecuteButton = new WeakReference<>(startExecute);
        this.stopExecuteButton = new WeakReference<>(stopExecute);
    }

    // onPreExecute() is used to do some UI operation before performing background tasks.
    @Override
    protected void onPreExecute() {
        textView.get().setText("Loading");
        startExecuteButton.get().setEnabled(false);
        stopExecuteButton.get().setEnabled(true);
    }

    @Override
    protected String doInBackground(Integer... inputParams) {

        StringBuffer retBuf = new StringBuffer();
        boolean loadComplete = false;

        try
        {
            int paramsLength = inputParams.length;
            if(paramsLength > 0) {
                Integer totalNumber = inputParams[0];
                int totalNumberInt = totalNumber.intValue();

                for(int i=0; i < totalNumberInt; i++)
                {
                    // First calculate progress value.
                    int progressValue = (i * 100 ) / totalNumberInt;

                    //Call publishProgress method to invoke onProgressUpdate() method.
                    publishProgress(progressValue);

                    // Sleep 0.2 seconds to demo progress clearly.
                    Thread.sleep(200);
                }

                loadComplete = true;
            }
        }catch(Exception ex)
        {
            Log.i(ASYNC_TASK_TAG, ex.getMessage());
        }finally {
            if(loadComplete) {
                // Load complete display message.
                retBuf.append("Load complete.");
            }else
            {
                // Load cancel display message.
                retBuf.append("Load canceled.");
            }
            return retBuf.toString();
        }
    }

    // onPostExecute() is used to update UI component and show the result after async task execute.
    @Override
    protected void onPostExecute(String result) {
        Log.i(ASYNC_TASK_TAG, "onPostExecute(" + result + ") is invoked.");
        // Show the result in log TextView object.
        textView.get().setText(result);

        progressBar.get().setProgress(0);

        startExecuteButton.get().setEnabled(true);
        stopExecuteButton.get().setEnabled(false);
    }

    // onProgressUpdate is used to update async task progress info.
    @Override
    protected void onProgressUpdate(Integer... values) {
        Log.i(ASYNC_TASK_TAG, "onProgressUpdate(" + values + ") is called");
        progressBar.get().setProgress(values[0]);
        textView.get().setText("loading..." + values[0] + "%");
    }

    // onCancelled() is called when the async task is cancelled.
    @Override
    protected void onCancelled(String result) {
        Log.i(ASYNC_TASK_TAG, "onCancelled(" + result + ") is invoked.");
        // Show the result in log TextView object.
        textView.get().setText(result);
        progressBar.get().setProgress(0);

        startExecuteButton.get().setEnabled(true);
        stopExecuteButton.get().setEnabled(false);
    }
}
