package mk.codeacademy.asynctaskexample;

import android.os.AsyncTask;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Random;

public class SimpleAsyncTask extends AsyncTask<Void, Void, String> {

    private WeakReference<TextView> textView;

    SimpleAsyncTask(TextView tv) {
        textView = new WeakReference<>(tv);
    }

    @Override
    protected void onPreExecute() {
        //UI Thread
        textView.get().setText(R.string.napping);
    }

    @Override
    protected String doInBackground(Void... voids) {

        Random rand = new Random();
        int num = rand.nextInt(11);

        int sleep = num * 200;

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Awake at last after sleeping for " + sleep + " milliseconds!";
    }

    @Override
    protected void onPostExecute(String result) {
        textView.get().setText(result);
    }
}
